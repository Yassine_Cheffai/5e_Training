package com.example.yacin.a5e_training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddStudent extends AppCompatActivity{

    String formation_result;
    String groupe_result;
    String nom_result;
    String prenom_result;
    String telephone_result;
    
    EditText nom;
    EditText prenom;
    EditText telephone;

    String msg;

    DBHandler dbHandler;

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHandler = new DBHandler(this,null,null,1);

        setContentView(R.layout.activity_add_student);
        nom = (EditText) findViewById(R.id.nom_input);
        prenom = (EditText) findViewById(R.id.prenom_input);
        telephone = (EditText) findViewById(R.id.input_telephone);

        telephone_result = telephone.getText().toString();
        prenom_result = prenom.getText().toString();
        nom_result = nom.getText().toString();
        msg = telephone_result+prenom_result+nom_result+formation_result+groupe_result;


        print_db_as_text();

        Spinner spinner_input_formation = (Spinner) findViewById(R.id.input_formation);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.formations_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner_input_formation.setAdapter(adapter);


        Spinner spinner_input_groupe = (Spinner) findViewById(R.id.input_groupe);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_2 = ArrayAdapter.createFromResource(this,
                R.array.groups_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner_input_groupe.setAdapter(adapter_2);



        spinner_input_formation.setOnItemSelectedListener(
                new Spinner.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        formation_result = (String) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );


        spinner_input_groupe.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        groupe_result = (String) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

    }


    public void add_student_btn_clicked(View view) {

        telephone_result = telephone.getText().toString();
        prenom_result = prenom.getText().toString();
        nom_result = nom.getText().toString();
        msg = telephone_result+prenom_result+nom_result+formation_result+groupe_result;
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

        Etudiant etudiant = new Etudiant(nom_result, prenom_result, telephone_result, formation_result
        , groupe_result, 0, 0);

        if ( (! telephone_result.equals("")) && (! nom_result.equals("")) && ! prenom_result.equals("")
                 && verif_telephone(telephone_result)){

            dbHandler.add_etudiant(etudiant);
            Toast.makeText(this, nom_result +" "+prenom_result +" a été ajouté", Toast.LENGTH_SHORT).show();
            Intent intent_reload = getIntent();
            finish();
            startActivity(intent_reload);
        }else{
            Toast.makeText(this, "valider les champs", Toast.LENGTH_LONG).show();
        }

        print_db_as_text();

    }

    public void print_db_as_text(){
        String temp_text = "";
        Etudiant[] etudiant_array = dbHandler.db_to_array();
        for (Etudiant etudiant:etudiant_array) {
            temp_text += etudiant.get_nom();
            temp_text += etudiant.get_prenom();
            temp_text += etudiant.get_telephone();
            temp_text += "\n";
        }

//        text.setText(temp_text);
    }

    public  boolean verif_telephone(String telephone){
        int  tel = Integer.parseInt(telephone);
        boolean result_verif;
        if ((tel <= 59000000 && tel >= 50000000) || (tel <= 29000000 && tel >= 20000000)
                || (tel <= 99000000 && tel >= 90000000) || (tel <= 49000000 && tel >= 40000000)){
            result_verif = true;
        }else{
            result_verif = false;
        }
        return result_verif;
    }

}

