package com.example.yacin.a5e_training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class StudentSearch extends AppCompatActivity {

    Spinner spinner_search_formation;
    Spinner spinner_search_groupe;

    String selected_formation;
    String selected_groupe;

    public static final String key_intent_formation = "com.example.yacin.a5e_training_key_intent_formation";
    public static final String key_intent_groupe = "com.example.yacin.a5e_training_key_key_intent_groupe";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_search);


        spinner_search_formation = (Spinner) findViewById(R.id.spinner_search_formation);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.formations_array,
                android.R.layout.simple_spinner_dropdown_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_search_formation.setAdapter(adapter);


        spinner_search_groupe = (Spinner) findViewById(R.id.spinner_search_groupe);
        ArrayAdapter<CharSequence> adapter_2 = ArrayAdapter.createFromResource(this, R.array.groups_array,
                android.R.layout.simple_spinner_dropdown_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_search_groupe.setAdapter(adapter_2);


        spinner_search_formation.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selected_formation = (String) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        selected_formation = (String) spinner_search_formation.getItemAtPosition(0);
        selected_groupe = (String) spinner_search_groupe.getItemAtPosition(0);

        spinner_search_groupe.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selected_groupe = (String) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                }
        );

//        Log.i("test", selected_formation + " " + selected_groupe);
    }

    public void search_btn_clicked(View view) {
//        Log.i("test", selected_formation + " " + selected_groupe);

        Intent intent = new Intent(this, EffectuerPresence.class);

        intent.putExtra(key_intent_formation, selected_formation);
        intent.putExtra(key_intent_groupe, selected_groupe);

        startActivity(intent);
    }


}
