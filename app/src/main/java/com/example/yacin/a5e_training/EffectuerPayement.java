package com.example.yacin.a5e_training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

//implementing onclicklistener
public class EffectuerPayement extends AppCompatActivity implements View.OnClickListener {

    //View Objects
    private Button buttonScan;
    private TextView textViewNom, textViewPrenom, textViewTelephone , textViewFormation,
            textViewGroupe, textViewNbrScienceEtudier, textViewNbrSciencePaye;

    DBHandler dbHandler = new DBHandler(this,null,null,1);

    Etudiant etudiant;


    //qr code scanner object
    private IntentIntegrator qrScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_effectuer_payement);

        //View objects
        buttonScan = (Button) findViewById(R.id.buttonScan);
        textViewNom = (TextView) findViewById(R.id.textViewNom);
        textViewPrenom = (TextView) findViewById(R.id.textViewPrenom);
        textViewTelephone = (TextView) findViewById(R.id.textViewtelephone);
        textViewFormation = (TextView) findViewById(R.id.textViewFormation);
        textViewGroupe = (TextView) findViewById(R.id.textViewGroupe);
        textViewNbrScienceEtudier = (TextView) findViewById(R.id.textViewNbrSCienceEtudier);
        textViewNbrSciencePaye = (TextView) findViewById(R.id.textViewNbrSciencePaye);

        //intializing scan object
        qrScan = new IntentIntegrator(this);

        //attaching onclick listener
        buttonScan.setOnClickListener(this);
    }

    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data

                //converting the data to json
//                    JSONObject obj = new JSONObject(result.getContents());
                String Searched_Telephone = result.getContents();
                Log.i("test_tag",Searched_Telephone);
                etudiant = dbHandler.search_etudiant_for_payement(Searched_Telephone);
                if (etudiant == null){
                    Toast.makeText(this, "scan qr code", Toast.LENGTH_SHORT).show();
                }
                else{
                    Log.i("test_tag", etudiant.get_prenom());
                    //setting values to textviews
//                    textViewName.setText(obj.getString("name"));
                    textViewNom.setText(etudiant.get_nom());
                    textViewPrenom.setText(etudiant.get_prenom());
                    textViewFormation.setText(etudiant.get_formation());
                    textViewTelephone.setText(etudiant.get_telephone());
                    textViewGroupe.setText(etudiant.get_groupe());
                    textViewNbrScienceEtudier.setText(String.valueOf(etudiant.get_nbr_scence_etudier()));
                    textViewNbrSciencePaye.setText(String.valueOf(etudiant.get_nbr_scence_paye()));
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        //initiating the qr code scan
        qrScan.initiateScan();
    }

    public void btn_paye_clicked(View view) {
        if (etudiant == null){
            Toast.makeText(this, "scan qr code", Toast.LENGTH_SHORT).show();
        }
        else {
            dbHandler.effectuer_payement(textViewTelephone.getText().toString());

            Intent intent_to_relod = getIntent();
            finish();
            startActivity(intent_to_relod);

            Toast.makeText(this, "payement pour " + etudiant.get_nom()+ " " + etudiant.get_prenom()
                    + " a été effectuer", Toast.LENGTH_SHORT).show();
        }

    }
}